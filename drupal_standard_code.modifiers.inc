<?php

/**
 * @file
 */
 
/**
 * Function to convert code into a string.
 */
function drupal_standard_code_modify_original_code($file, $drupal_standard_code_module_file) {

  $dir_path = variable_get('drupal_standard_code_select_directory', 'sites/all/modules');
  $drupal_standard_code_modulepath = $dir_path .'/'. $drupal_standard_code_module_file;
  $get_subdirectories = scandir($drupal_standard_code_modulepath);
  $subdirectory_list = array();

    foreach ($get_subdirectories as $subdirectory) {
      if(is_dir($dir_path .'/'. $drupal_standard_code_module_file .'/'. $subdirectory)) {
        $subdirectory_list[] = $subdirectory;
      }
    }

  $exclude_from_subdirectory_list_array = array('.', '..', '.svn', 'CVS'); // files/directories to exclude from the list.
  $subdirectory_list=array_diff($subdirectory_list, $exclude_from_subdirectory_list_array);

    foreach ($subdirectory_list as $subdir) {
      if ($subdir!=NULL && file_exists($drupal_standard_code_modulepath .'/'. $subdir .'/'. $file)) {
        $file_to_modify = file_get_contents($drupal_standard_code_modulepath .'/'. $subdir .'/'. $file);
      }
      elseif (file_exists($drupal_standard_code_modulepath .'/'. $file)) {
        $file_to_modify = file_get_contents($drupal_standard_code_modulepath .'/'. $file);
      }
    }

    drupal_standard_code_execute_modifiers($file_to_modify, $file, $drupal_standard_code_module_file);
}


/**
 * Function to modify/standardize code.
 * Be very careful not to change the sequence of statements. It may lead to undesirable results.
 */
function drupal_standard_code_execute_modifiers($file_to_modify, $file, $drupal_standard_code_module_file) {

$drupal_standard_code_modifier = variable_get('drupal_standard_code_select_modifications-options', 0);

  if (!$drupal_standard_code_modifier['spaces_after_comma']==0) {
    $file_to_modify = str_replace(' ,', ',', $file_to_modify);
    $file_to_modify = str_replace(', ', ',', $file_to_modify);
    $file_to_modify = str_replace(',', ', ', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['truncate_spaces_between_string_concat']==0) {
    $file_to_modify = preg_replace("/\'\s*\./m", "'.", $file_to_modify);
    $file_to_modify = preg_replace("/\.\s*\'/m", ".'", $file_to_modify);
    $file_to_modify = preg_replace('/\"\s*\./m', '".', $file_to_modify);
    $file_to_modify = preg_replace('/\.\s*\"/m', '."', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['spaces_between_nonstring_concat']==0) {
    $file_to_modify = str_replace('.$', '. $', $file_to_modify);
    $file_to_modify = str_replace('.(', '. (', $file_to_modify);
    $file_to_modify = str_replace('.[', '. [', $file_to_modify);
    $file_to_modify = str_replace(').', ') .', $file_to_modify);
    $file_to_modify = str_replace('].', '] .', $file_to_modify);
    $file_to_modify = str_replace(' .=', '.=', $file_to_modify);
    $file_to_modify = str_replace('.=', ' .=', $file_to_modify);
    $file_to_modify = str_replace("'. ", "'.", $file_to_modify);
    $file_to_modify = str_replace("'.", "'. ", $file_to_modify);
    $file_to_modify = str_replace(" .'", ".'", $file_to_modify);
    $file_to_modify = str_replace(".'", " .'", $file_to_modify);
    $file_to_modify = str_replace('". ', '".', $file_to_modify);
    $file_to_modify = str_replace('".', '". ', $file_to_modify);
    $file_to_modify = str_replace(' ."', '."', $file_to_modify);
    $file_to_modify = str_replace('."', ' ."', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['replace_tabs']==0) {
    $file_to_modify = preg_replace('/\t/', '  ', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['spaces_after_ctrl_func']==0) {
    $file_to_modify = str_replace('if(', 'if (', $file_to_modify);
    $file_to_modify = str_replace('foreach(', 'foreach (', $file_to_modify);
    $file_to_modify = str_replace('while(', 'while (', $file_to_modify);
    $file_to_modify = str_replace('switch(', 'switch (', $file_to_modify);
    $file_to_modify = str_replace('for(', 'for (', $file_to_modify);
    $file_to_modify = str_replace('elseif(', 'elseif (', $file_to_modify);
    $file_to_modify = str_replace('else(', 'else (', $file_to_modify);
    $file_to_modify = str_replace('}elseif(', '}elseif (', $file_to_modify);
    $file_to_modify = str_replace('}else(', '}else (', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['spaces_after_closing_bracket_paranthesis']==0) {
    $file_to_modify = str_replace('){', ') {', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['array_pointers']==0) {
    $file_to_modify = str_replace(' => ', '=>', $file_to_modify);
    $file_to_modify = str_replace('=>', ' => ', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['correct_html_break']==0) {
    $file_to_modify = str_replace('<br>', '<br />', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['replace_php_constants_strings']==0) {
    $file_to_modify = str_ireplace('true', 'TRUE', $file_to_modify);
    $file_to_modify = str_ireplace('false', 'FALSE', $file_to_modify);
    $file_to_modify = str_ireplace('null', 'NULL', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['replace_string_functions']==0) {
    $file_to_modify = preg_replace('/\bstrlen/', 'drupal_strlen', $file_to_modify);
    $file_to_modify = preg_replace('/\bstrtoupper/', 'drupal_strtoupper', $file_to_modify);
    $file_to_modify = preg_replace('/\bstrtolower/', 'drupal_strtolower', $file_to_modify);
    $file_to_modify = preg_replace('/\bucfirst/', 'drupal_ucfirst', $file_to_modify);
    $file_to_modify = preg_replace('/\bsubstr/', 'drupal_substr', $file_to_modify);
  }
  /*
  if (!$drupal_standard_code_modifier['uppercase_sql_keywords']==0) {
    $file_to_modify = str_ireplace('( \'select', '( \'SELECT', $file_to_modify);
    $file_to_modify = str_ireplace('( "select', '( "SELECT', $file_to_modify);
    $file_to_modify = str_ireplace('(\'select', '(\'SELECT', $file_to_modify);
    $file_to_modify = str_ireplace('("select', '("SELECT', $file_to_modify);
    $file_to_modify = str_ireplace('( \'update', '( \'UPDATE', $file_to_modify);
    $file_to_modify = str_ireplace('( "update', '( "UPDATE', $file_to_modify);
    $file_to_modify = str_ireplace('(\'update', '(\'UPDATE', $file_to_modify);
    $file_to_modify = str_ireplace('("update', '("UPDATE', $file_to_modify); 
 //   $file_to_modify = str_ireplace('where', 'WHERE', $file_to_modify);  //potential problem. May replace sub-strings in function/variable names 
 //   $file_to_modify = str_ireplace('from', 'FROM', $file_to_modify);  //potential problem. May replace sub-strings in function/variable names
    $file_to_modify = str_ireplace('insert into', 'INSERT INTO', $file_to_modify);
    $file_to_modify = str_ireplace('left join', 'LEFT JOIN', $file_to_modify);
    $file_to_modify = str_ireplace('right join', 'RIGHT JOIN', $file_to_modify);
    $file_to_modify = str_ireplace('inner join', 'INNER JOIN', $file_to_modify);
    $file_to_modify = str_ireplace('outer join', 'OUTER JOIN', $file_to_modify);
 //   $file_to_modify = str_ireplace('join', 'JOIN', $file_to_modify);  //potential problem. May replace sub-strings in function/variable names
  }
  */
  if (!$drupal_standard_code_modifier['space_at_start_comment']==0) {
    $file_to_modify = preg_replace('/^\s*\*/m', ' *', $file_to_modify);
  }

  if (!$drupal_standard_code_modifier['truncate_trailing_spaces']==0) {
    $file_to_modify = preg_replace('/\s*$/m', '', $file_to_modify);
  }

drupal_standard_code_save_modified_code($file_to_modify, $file, $drupal_standard_code_module_file);
}


function drupal_standard_code_save_modified_code($file_to_modify, $file, $drupal_standard_code_module_file) {

$drupal_standard_code_modified_file_folder = file_directory_path() .'/DSC_'. $drupal_standard_code_module_file;

  if (!file_exists(file_directory_path() .'/DSC_'. $drupal_standard_code_module_file)) {
    mkdir(file_directory_path() .'/DSC_'. $drupal_standard_code_module_file);
    chmod($drupal_standard_code_modified_file_folder, 0777);
    drupal_set_message('Created '. $drupal_standard_code_modified_file_folder, 'status');
  }
  
  file_save_data($file_to_modify, $file, FILE_EXISTS_REPLACE);
  $drupal_standard_code_modified_file=file_directory_path() .'/'. $file;
  chmod($drupal_standard_code_modified_file, 0777);

  file_move($drupal_standard_code_modified_file, $drupal_standard_code_modified_file_folder, FILE_EXISTS_RENAME);

  drupal_set_message($file .' has been modified successfully.', 'status');
}

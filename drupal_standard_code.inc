<?php
/**
 * @file
 * This files contains all the include functions.
 */

/**
 * Function to select all the directories inside a directory.
 */
function drupal_standard_code_select_directories_array() {
  $dir_path = variable_get('drupal_standard_code_select_directory', 'sites/all/modules');
  $dir_list_array = scandir($dir_path);
  $files_in_dir_list = array();
  foreach ($dir_list_array as $file) {
    if(!is_dir($dir_path .'/'. $file)){
      $files_in_dir_list[] = $file;
    }
  }
  $exclude_from_dir_list_array = array_merge(array('.', '..', '.svn', 'CVS'), $files_in_dir_list); // files/directories to exclude from the list.
  $module_list=array_diff($dir_list_array, $exclude_from_dir_list_array);
  return $module_list;
}

/**
 * Function to select all the files inside a directory.
 */
function drupal_standard_code_select_files_array($drupal_standard_code_module_name) {
  $file_list_extensions_array = file_scan_directory(drupal_get_path('module', $drupal_standard_code_module_name), 'module|inc|install|tpl\.php', array('.', '..', 'CVS'), 0, TRUE, 'basename', 0, 0);
  $filename = array();
  foreach ($file_list_extensions_array as $files) {
    $filename[$files->basename] = $files->basename;
  }
  return $filename;
}
